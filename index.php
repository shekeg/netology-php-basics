<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <link href="https://fonts.googleapis.com/css?family=Exo+2" rel="stylesheet">
  <link rel="stylesheet" href="main.css">
</head>
<body>
  <div class="wrap">
    <div class="container">
      <div class="banner">
        <h1 class="title">Число Фибоначчи</h1>
        <img class="photo-fibonacci" src="fibonacci.jpg" alt="Fibonacci">
      </div>
      <div class="content">
        <form class="form-main" action="index.php">
          <label for="user-data">Введите число: </label>
          <input class="field" type="text" name="user_data" id="user-data" autocomplete="off">
          <input class="submit" type="submit" value="Проверить">
        </form>
        <?php
          if (isset($_GET['user_data']) and ($_GET['user_data'] != '')) {
            $userData = $_GET['user_data'];
            $first = 1;
            $second = 1;
            echo '<p>Введено число: ' . $userData . '</p>';
            echo '<p>Последовательность чисел:</p>';
            while (true) {
              if ($first > $userData) {
                echo '<p class="result-red">Число не входит в числовой ряд</p>';
                break;
              }
              elseif ($first == $userData) {
                echo $userData . '<br>';
                echo '<p class="result-green">Задуманное число входит в числовой ряд</p>';
                break;
              }
              elseif ($first != $userData){
                $third = $first;
                $first += $second;
                $second = $third;
                echo $second . '<br>';
              }
            }
          } else {
            echo '<p>Число не введено</p>';
          }
        ?>
      </div>
    </div>
  </div>
</body>
</html>